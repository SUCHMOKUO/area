package com.example.area.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QArea is a Querydsl query type for Area
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QArea extends EntityPathBase<Area> {

    private static final long serialVersionUID = -1827075193L;

    public static final QArea area = new QArea("area");

    public final StringPath areaCode = createString("areaCode");

    public final StringPath cityCode = createString("cityCode");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath lat = createString("lat");

    public final NumberPath<Integer> level = createNumber("level", Integer.class);

    public final StringPath lng = createString("lng");

    public final StringPath mergerName = createString("mergerName");

    public final StringPath name = createString("name");

    public final NumberPath<Integer> parentId = createNumber("parentId", Integer.class);

    public final StringPath pinyin = createString("pinyin");

    public final StringPath shortName = createString("shortName");

    public final NumberPath<Integer> sourceId = createNumber("sourceId", Integer.class);

    public final StringPath zipCode = createString("zipCode");

    public QArea(String variable) {
        super(Area.class, forVariable(variable));
    }

    public QArea(Path<? extends Area> path) {
        super(path.getType(), path.getMetadata());
    }

    public QArea(PathMetadata metadata) {
        super(Area.class, metadata);
    }

}

