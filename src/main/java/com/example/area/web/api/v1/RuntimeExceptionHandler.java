package com.example.area.web.api.v1;

import com.example.area.domain.exceptions.NotFoundException;
import com.example.area.service.responses.NotFoundResponse;
import com.example.area.service.responses.Response;
import com.example.area.service.responses.ServerErrorResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author xing.liu
 */
@ControllerAdvice
public class RuntimeExceptionHandler {
    @ExceptionHandler
    @ResponseBody
    public Response handle(RuntimeException e) {
        if (e instanceof NotFoundException) {
            return new NotFoundResponse(e.getMessage());
        }
        return new ServerErrorResponse("unknown error");
    }
}