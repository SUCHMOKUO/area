package com.example.area.web.api.v1;

import com.example.area.domain.Area;
import com.example.area.service.AreaService;
import com.example.area.service.responses.Response;
import com.example.area.service.responses.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @author xing.liu
 */
@Api(tags = "地区信息API")
@RestController
@RequestMapping("/api/v1/areas")
@CrossOrigin("*")
public class AreaController {
    private final AreaService areaService;

    @Autowired
    public AreaController(AreaService areaService) {
        this.areaService = areaService;
    }

    @ApiOperation(value = "获取指定地区信息")
    @ApiImplicitParam(name = "level", value = "区域level")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = Result.class),
        @ApiResponse(code = 404, message = "Not Found", response = Result.class)
    })
    @GetMapping()
    public Response getAllAreas(
        @RequestParam Optional<Integer> level
    ) {
        return areaService.getAreas(level);
    }

    @ApiOperation(value = "根据id获取地区信息")
    @ApiImplicitParam(name = "id", value = "地区id", required = true)
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = Result.class),
        @ApiResponse(code = 404, message = "Not Found", response = Result.class)
    })
    @GetMapping(path = "{id}")
    public Response getAreaById(
        @NonNull @PathVariable Integer id
    ) {
        return areaService.getAreaById(id);
    }

    @ApiOperation(value = "新增一个地区")
    @ApiImplicitParam(name = "area", value = "地区信息", dataType = "Area", required = true)
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created", response = Result.class),
    })
    @PostMapping()
    public Response createArea(
        @NonNull @RequestBody Area area
    ) {
        return areaService.createArea(area);
    }

    @ApiOperation(value = "更新一个地区的信息")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "需要更新地区的id", required = true),
        @ApiImplicitParam(name = "area", value = "需要更新的地区信息", dataType = "Area", required = true)
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = Result.class),
        @ApiResponse(code = 404, message = "Not Found", response = Result.class)
    })
    @PutMapping(path = "{id}")
    public Response updateArea(
        @NonNull @PathVariable Integer id,
        @NonNull @RequestBody Area area
    ) {
        return areaService.updateArea(id, area);
    }

    @ApiOperation(value = "删除一个地区的信息")
    @ApiImplicitParam(name = "id", value = "需要删除地区的id", required = true)
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = Result.class),
        @ApiResponse(code = 404, message = "Not Found", response = Result.class)
    })
    @DeleteMapping(path = "{id}")
    public Response deleteArea(
        @NonNull @PathVariable Integer id
    ) {
        return areaService.deleteArea(id);
    }
}
