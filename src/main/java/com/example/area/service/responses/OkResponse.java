package com.example.area.service.responses;

import org.springframework.http.HttpStatus;

/**
 * @author xing.liu
 */
public class OkResponse extends Response {
    private final static HttpStatus CODE = HttpStatus.OK;

    public OkResponse(Object data) {
        super(CODE, "OK", data);
    }

    public OkResponse(Object data, String message) {
        super(CODE, message, data);
    }
}
