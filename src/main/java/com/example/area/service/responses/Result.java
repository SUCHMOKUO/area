package com.example.area.service.responses;

import lombok.Data;

/**
 * @author xing.liu
 */
@Data
public class Result {
    /**
     * status code
     */
    private final int code;

    /**
     * response message
     */
    private final String message;

    /**
     * response data
     */
    private final Object data;
}
