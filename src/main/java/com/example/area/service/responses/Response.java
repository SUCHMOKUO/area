package com.example.area.service.responses;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author xing.liu
 */
public class Response extends ResponseEntity<Result> {
    public Response(HttpStatus status, String message, Object data) {
        super(new Result(status.value(), message, data), status);
    }
}
