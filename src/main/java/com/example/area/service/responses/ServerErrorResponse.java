package com.example.area.service.responses;

import org.springframework.http.HttpStatus;

/**
 * @author xing.liu
 */
public class ServerErrorResponse extends Response {
    private final static HttpStatus CODE = HttpStatus.INTERNAL_SERVER_ERROR;

    public ServerErrorResponse(String message) {
        super(CODE, message, null);
    }
}
