package com.example.area.service.responses;

import org.springframework.http.HttpStatus;

/**
 * @author xing.liu
 */
public class CreatedResponse extends Response {
    private final static HttpStatus CODE = HttpStatus.CREATED;

    public CreatedResponse(Object data, String message) {
        super(CODE, message, data);
    }
}
