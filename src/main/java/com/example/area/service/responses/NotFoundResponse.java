package com.example.area.service.responses;

import org.springframework.http.HttpStatus;

/**
 * @author xing.liu
 */
public class NotFoundResponse extends Response {
    private final static HttpStatus CODE = HttpStatus.NOT_FOUND;

    public NotFoundResponse(String message) {
        super(CODE, message, null);
    }
}
