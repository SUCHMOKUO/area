package com.example.area.service;

import com.example.area.domain.Area;
import com.example.area.domain.AreaRepository;
import com.example.area.service.responses.CreatedResponse;
import com.example.area.service.responses.OkResponse;
import com.example.area.service.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author xing.liu
 */
@Service
public class AreaService {
    private final AreaRepository areaRepository;

    @Autowired
    public AreaService(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    public Response getAllAreas() {
        List<Area> areas = areaRepository.getAllAreas();
        return new OkResponse(areas);
    }

    public Response getAreasByLevel(Integer level) {
        List<Area> areaByLevel = areaRepository.getAreasByLevel(level);
        return new OkResponse(areaByLevel);
    }

    public Response getAreas(Optional<Integer> level) {
        if (level.isPresent()) {
            return getAreasByLevel(level.get());
        }
        return getAllAreas();
    }

    public Response getAreaById(Integer id) {
        Area area = areaRepository.getAreaById(id);
        return new OkResponse(area);
    }

    public Response createArea(Area area) {
        Area savedArea = areaRepository.createArea(area);
        CreatedResponse createdResponse = new CreatedResponse(savedArea, "创建成功");
        return createdResponse;
    }

    public Response updateArea(Integer id, Area area) {
        Area updatedArea = areaRepository.updateArea(id, area);
        return new OkResponse(updatedArea, "更新成功");
    }

    public Response deleteArea(Integer id) {
        Area deletedArea = areaRepository.deleteArea(id);
        return new OkResponse(deletedArea, "删除成功");
    }
}
