package com.example.area.domain;

import com.example.area.domain.exceptions.NotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xing.liu
 */
@Repository
public interface AreaRepository extends JpaRepository<Area, Integer>,
    QuerydslPredicateExecutor<Area> {
    /**
     * 获取所有地区信息
     *
     * @return list of all area
     */
    default List<Area> getAllAreas() {
        List<Area> areas = this.findAll();
        if (0 == areas.size()) {
            throw new NotFoundException("没有数据");
        }
        return areas;
    }

    /**
     * 获取所有指定level地区
     *
     * @param level
     * @return list of area of that level
     */
    default List<Area> getAreasByLevel(Integer level) {
        List<Area> areas = new ArrayList<>();
        QArea qArea = QArea.area;
        this.findAll(qArea.level.eq(level)).forEach(areas::add);
        if (0 == areas.size()) {
            throw new NotFoundException("没有该level地区");
        }
        return areas;
    }

    /**
     * 获取指定id地区
     *
     * @param id
     * @return area of that id
     */
    default Area getAreaById(Integer id) {
        return this.findById(id)
            .orElseThrow(() -> new NotFoundException("没有该地区"));
    }

    /**
     * 创建新地区并返回
     *
     * @param area area to create
     * @return created area
     */
    default Area createArea(Area area) {
        return this.save(area);
    }

    /**
     * 更新地区信息
     *
     * @param id area id
     * @param area area info
     * @return updated area
     */
    default Area updateArea(Integer id, Area area) {
        if (!this.existsById(id)) {
            throw new NotFoundException("不能更新不存在的数据");
        }
        area.setId(id);
        return this.save(area);
    }

    /**
     * 删除指定id的地区
     *
     * @param id area id
     * @return deleted area
     */
    default Area deleteArea(Integer id) {
        Area areaToDelete = this.findById(id)
            .orElseThrow(() -> new NotFoundException("不能删除不存在的数据"));

        this.deleteById(id);
        return areaToDelete;
    }
}