package com.example.area.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author xing.liu
 */
@ApiModel("Area")
@NoArgsConstructor
@Data
@Entity
@Table(name = "areas")
public class Area {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "source_id")
    @JsonProperty("source_id")
    private Integer sourceId;

    private Integer level;

    @Column(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    private String pinyin;

    @ApiModelProperty("纬度")
    private String lng;

    @ApiModelProperty("经度")
    private String lat;

    @Column(name = "area_code")
    @JsonProperty("area_code")
    private String areaCode;

    private String name;

    @Column(name = "merger_name")
    @JsonProperty("merger_name")
    private String mergerName;

    @Column(name = "city_code")
    @JsonProperty("city_code")
    private String cityCode;

    @Column(name = "short_name")
    @JsonProperty("short_name")
    private String shortName;

    @Column(name = "zip_code")
    @JsonProperty("zip_code")
    private String zipCode;
}
