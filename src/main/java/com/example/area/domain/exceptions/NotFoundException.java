package com.example.area.domain.exceptions;

/**
 * @author xing.liu
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
}
