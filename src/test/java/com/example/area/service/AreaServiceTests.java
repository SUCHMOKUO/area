package com.example.area.service;

import com.example.area.domain.Area;
import com.example.area.domain.AreaRepository;
import com.example.area.domain.exceptions.NotFoundException;
import com.example.area.service.responses.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AreaServiceTests {
    private static AreaRepository areaRepository;
    private AreaService areaService;

    private static int getRandomId() {
        Random r = new Random();
        return r.nextInt(100);
    }

    private static Object getResponseData(Response res) {
        return res.getBody().getData();
    }

    private static int getResponseCode(Response res) {
        return res.getBody().getCode();
    }

    @BeforeClass
    public static void initRepository() {
        areaRepository = mock(AreaRepository.class);
    }

    @Before
    public void initService() {
        reset(areaRepository);
        areaService = new AreaService(areaRepository);
    }

    @Test
    public void testGetAllAreas() {
        List<Area> mockAreas = new ArrayList<>();
        mockAreas.add(new Area());
        mockAreas.add(new Area());
        when(areaRepository.getAllAreas()).thenReturn(mockAreas);
        Response res = areaService.getAllAreas();
        List<Area> allAreas = (List<Area>) getResponseData(res);
        Assert.assertEquals(200, getResponseCode(res));
        Assert.assertEquals(mockAreas.size(), allAreas.size());
        verify(areaRepository).getAllAreas();
    }

    @Test(expected = NotFoundException.class)
    public void testGetAllAreasNotExist() {
        when(areaRepository.getAllAreas())
            .thenThrow(NotFoundException.class);
        areaService.getAllAreas();
        verify(areaRepository).getAllAreas();
    }

    @Test
    public void testGetAreasByLevel() {
        List<Area> mockAreas = new ArrayList<>();
        mockAreas.add(new Area());
        Integer level = 0;
        when(areaRepository.getAreasByLevel(level)).thenReturn(mockAreas);
        Response res = areaService.getAreasByLevel(level);
        List<Area> allAreas = (List<Area>) getResponseData(res);
        Assert.assertEquals(200, getResponseCode(res));
        Assert.assertEquals(mockAreas.size(), allAreas.size());
        verify(areaRepository).getAreasByLevel(level);
    }

    @Test(expected = NotFoundException.class)
    public void testGetAreasByLevelNotExist() {
        Integer level = 10;
        when(areaRepository.getAreasByLevel(level))
            .thenThrow(NotFoundException.class);
        areaService.getAreasByLevel(level);
        verify(areaRepository).getAreasByLevel(level);
    }

    @Test
    public void testGetAreaById() {
        Area mockArea = new Area();
        Integer id = getRandomId();
        mockArea.setId(id);
        when(areaRepository.getAreaById(id)).thenReturn(mockArea);
        Response res = areaService.getAreaById(id);
        Area area = (Area) getResponseData(res);
        Assert.assertEquals(area, mockArea);
        Assert.assertEquals(200, getResponseCode(res));
        verify(areaRepository).getAreaById(id);
    }

    @Test(expected = NotFoundException.class)
    public void testGetAreaByIdNotExist() {
        Integer id = getRandomId();
        when(areaRepository.getAreaById(id))
            .thenThrow(NotFoundException.class);
        areaService.getAreaById(id);
        verify(areaRepository).getAreaById(id);
    }

    @Test
    public void testCreateArea() {
        Area newArea = new Area();
        when(areaRepository.createArea(newArea)).thenReturn(newArea);
        Response res = areaService.createArea(newArea);
        Area createdArea = (Area) getResponseData(res);
        Assert.assertEquals(201, getResponseCode(res));
        Assert.assertEquals(createdArea, newArea);
        verify(areaRepository).createArea(newArea);
    }

    @Test
    public void testUpdateArea() {
        Area area = new Area();
        Integer id = getRandomId();
        area.setId(id);
        when(areaRepository.updateArea(id, area)).thenReturn(area);
        Response res = areaService.updateArea(id, area);
        Area updatedArea = (Area) getResponseData(res);
        Assert.assertEquals(200, getResponseCode(res));
        Assert.assertEquals(updatedArea, area);
        verify(areaRepository).updateArea(id, area);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateAreaNotExist() {
        Integer nonExistId = getRandomId();
        Area nonExistArea = new Area();
        nonExistArea.setId(nonExistId);
        when(areaRepository.updateArea(nonExistId, nonExistArea))
            .thenThrow(NotFoundException.class);
        areaService.updateArea(nonExistId, nonExistArea);
        verify(areaRepository).updateArea(nonExistId, nonExistArea);
    }

    @Test
    public void testDeleteArea() {
        Integer id = getRandomId();
        Area areaToDelete = new Area();
        when(areaRepository.deleteArea(id)).thenReturn(areaToDelete);
        Response res = areaService.deleteArea(id);
        Assert.assertEquals(200, getResponseCode(res));
        Assert.assertEquals(areaToDelete, getResponseData(res));
        verify(areaRepository).deleteArea(id);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteAreaNotExist() {
        Integer id = getRandomId();
        when(areaRepository.deleteArea(id)).thenThrow(NotFoundException.class);
        areaService.deleteArea(id);
        verify(areaRepository).deleteArea(id);
    }
}
