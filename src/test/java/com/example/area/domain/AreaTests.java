package com.example.area.domain;

import org.junit.Assert;
import org.junit.Test;

public class AreaTests {
    @Test
    public void testAreas() {
        Area area = new Area();
        String name = "test_name";
        area.setName(name);
        Assert.assertEquals(name, area.getName());
    }
}
