package com.example.area.domain;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AreaRepositoryTests {
    @Autowired
    private AreaRepository areaRepository;

    private String testName;
    private Integer testLevel = 1;
    private Integer testId;

    @Before
    public void addData() {
        Area area = new Area();
        testName = String.valueOf(Math.random());
        area.setName(testName);
        area.setLevel(testLevel);
        areaRepository.save(area);
        testId = area.getId();
    }

    @After
    public void delData() {
        areaRepository.deleteAll();
    }

    @Test
    public void getAllAreas() {
        List<Area> areas = areaRepository.getAllAreas();
        Assert.assertEquals(1, areas.size());
    }

    @Test
    public void getAreasByLevel() {
        List<Area> areas = areaRepository.getAreasByLevel(testLevel);
        Assert.assertEquals(1, areas.size());
    }

    @Test
    public void getAreaById() {
        Area areaFromDb = areaRepository.getAreaById(testId);
        Assert.assertEquals(areaFromDb.getName(), testName);
    }

    @Test
    public void createArea() {
        Area area = new Area();
        areaRepository.createArea(area);
        Assert.assertNotNull(area.getId());
    }

    @Test
    public void updateArea() {
        Area area = new Area();
        String newName = "new name";
        area.setName(newName);
        Area updatedArea = areaRepository.updateArea(testId, area);
        Assert.assertEquals(updatedArea.getName(), newName);
    }

    @Test
    public void deleteArea() {
        areaRepository.deleteArea(testId);
        Assert.assertFalse(areaRepository.existsById(testId));
    }
}
