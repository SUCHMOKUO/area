#!/usr/bin/env sh

. ./conf.sh

mysql_root_passwd=$1
mysql_passwd=$2

if [ -z $mysql_root_passwd ] || [ -z $mysql_passwd ]; then
    echo "mysql_root_passwd and mysql_passwd needed! ENV: MYSQL_ROOT_PASSWORD, MYSQL_PASSWORD"
    exit 1
fi

image_name="$registry/$repo_name/area"

### create area-data volume ###
if [ ! "$(docker volume ls | grep -w $mysql_volume)" ]; then
    docker volume create $mysql_volume
fi

### create network area-net ###
if [ ! "$(docker network ls | grep -w $network_name)" ]; then
    docker network create $network_name
fi

### start area container ###
if [ -n "$(docker container ls -a | grep -w $area_name)" ]; then
    docker container rm -f $area_name
fi

docker run \
	--name $area_name \
	--network $network_name \
	-e MYSQL_PASSWORD=$mysql_passwd \
	-d \
	$image_name

### create mysql container ###
if [ -n "$(docker container ls -a | grep -w $mysql_name)" ]; then
    docker container rm -f $mysql_name
fi

docker run \
	--name $mysql_name \
	--network $network_name \
	--mount source=$mysql_volume,target=/var/lib/mysql \
	-e MYSQL_ROOT_PASSWORD=$mysql_root_passwd \
	-d \
	mysql

### create nginx container ###
if [ -n "$(docker container ls -a | grep -w $nginx_name)" ]; then
    docker container rm -f $nginx_name
fi

docker run \
	--name $nginx_name \
	--network $network_name \
	-v $nginx_content_path:/usr/share/nginx/html \
	-v $nginx_conf_path:/etc/nginx/nginx.conf \
	-p "0.0.0.0:$nginx_port":80 \
	-d \
	nginx:alpine