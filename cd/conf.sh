#!/usr/bin/env sh

### image source config ###
registry="registry.gitlab.com"
repo_name="suchmokuo/area"

### docker network config ###
network_name="area_net"

### nginx config ###
nginx_name="nginx"
nginx_content_path="$(pwd)/nginx/html"
nginx_conf_path="$(pwd)/nginx/nginx.conf"
nginx_port=80

### area config ###
area_name="area"
area_port=3000

### mysql config ###
mysql_name="area_mysql"
mysql_volume="area_data"