#!/usr/bin/env sh

server=$1
user=$2
key=$3
mysql_root_passwd=$4
mysql_passwd=$5

if [ -z $server ]; then
    echo "need server, ENV: PROD_SERVER"
    exit 1
fi

if [ -z $user ]; then
    echo "need user, ENV: PROD_SERVER_USER"
    exit 1
fi

if [ -z $key ]; then
    echo "need key, ENV: PROD_SERVER_KEY"
    exit 1
fi

if [ -z $mysql_root_passwd ]; then
    echo "need mysql_root_passwd, ENV: MYSQL_ROOT_PASSWORD"
    exit 1
fi

if [ -z $mysql_passwd ]; then
    echo "need mysql_passwd, ENV: MYSQL_PASSWORD"
    exit 1
fi

dir="/home/$user/area"
cp -rf ./* $dir
cd $dir
chmod +x *.sh
./run.sh $mysql_root_passwd $mysql_passwd