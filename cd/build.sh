#!/usr/bin/env sh

. ./conf.sh

user=$1
passwd=$2

if [ -z $passwd ] || [ -z $user ]; then
    echo "gitlab username and password needed! ENV: GITLAB_USER, GITLAB_PASSWD"
    exit 1
fi

image_name="$registry/$repo_name/area"

### build image ###
docker build -t $image_name .

### login ###
docker login -u $user -p $passwd $registry

### push image ###
docker push $image_name